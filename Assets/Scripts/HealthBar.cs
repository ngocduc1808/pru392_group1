using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class HealthBar : MonoBehaviour
{

    public Image fillBar;
    public TextMeshProUGUI valueText;

    public void UpdateBar(int currentValue, int maxvalue)
    {
        fillBar.fillAmount= (float) currentValue/ (float)maxvalue;
        valueText.text= currentValue.ToString()+ " / "+ maxvalue.ToString();
    }
}
