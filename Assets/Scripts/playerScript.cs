using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking.Types;
using UnityEngine.UI;
using TMPro;
using Unity.VisualScripting;

public class playerScript : MonoBehaviour
{
    [SerializeField]private float speed;
    [SerializeField] int maxHealth;
    [SerializeField] private TextMeshProUGUI appleCount;
    int currentHealth;
    public HealthBar healthBar;
    public int damage;
    private int apple = 0;
    //public float force = 450f;
    //public float suction_power = 5f;
    //public float low_jump = 5f;

    //private bool touch = true;
    private bool check_right = true;
    private bool grounded;
    //private float speed = 0f;

    private Rigidbody2D rigidbody;
    private Animator animator;


    private float horizontalInput;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();

    }
    private void Start()
    {
        currentHealth = maxHealth;
        healthBar.UpdateBar(currentHealth, maxHealth);
    }
    void Update()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        rigidbody.velocity =  new Vector2(horizontalInput*speed,rigidbody.velocity.y);

        if (horizontalInput > 0.01f && !check_right)
        {
            face_turn();
        }
        else if (horizontalInput < -0.01f && check_right)
        {
            face_turn();
        }

        if (Input.GetKey(KeyCode.Space) && grounded)
        {
            jump();
        }
        animator.SetBool("run", horizontalInput != 0);
        animator.SetBool("grounded", grounded);

        if (Input.GetKeyDown(KeyCode.V))
        {
            takeDamage(damage);
        }
        
    }
    void takeDamage(int damage)
    {
        currentHealth -= damage;
        if(currentHealth <= 0)
        {
            Destroy(this.gameObject);
        }
        healthBar.UpdateBar(currentHealth, maxHealth);

    }
    void face_turn()
    {
        check_right = !check_right;
        Vector2 turn = transform.localScale;
        turn.x *= -1;
        transform.localScale = turn;
    }
    void jump()
    {
        animator.SetTrigger("jump");
        rigidbody.velocity = new Vector2(rigidbody.velocity.x, speed);
        grounded = false;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "ground")
        {
            grounded = true;
        }
    }
    public bool canAttack()
    {
        return horizontalInput == 0 && grounded == true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("fruit"))
        {
            Destroy(collision.gameObject);
            apple++;
            appleCount.text = "Apple: " + apple;
        }
        else if (collision.gameObject.CompareTag("trap"))
        {
            takeDamage(damage);
        }
    }

    

    //private void FixedUpdate()
    //{
    //    move();
    //}
    //void move()
    //{
    //    float left_right = Input.GetAxis("Horizontal");
    //    rigidbody.velocity = new Vector2(speedRun * left_right, rigidbody.velocity.y);
    //    //speedRun = Mathf.Abs(speedRun * left_right);
    //    speed = Mathf.Abs(speedRun * left_right);

    //    if (left_right > 0 && !check_right)
    //    {
    //        face_turn();
    //    }
    //    if (left_right < 0 && check_right)
    //    {
    //        face_turn();
    //    }
    //}
    //void face_turn()
    //{
    //    check_right = !check_right;
    //    Vector2 turn = transform.localScale;
    //    turn.x *= -1;
    //    transform.localScale = turn;

    //}
    //void jump()
    //{
    //    if (Input.GetKeyDown(KeyCode.UpArrow) && touch == true)
    //    {
    //        rigidbody.AddForce(Vector2.up * force);
    //        touch = false;
    //    }
    //    if (rigidbody.velocity.y < 0)
    //    {
    //        rigidbody.velocity += Vector2.up * Physics2D.gravity.y * (suction_power - 1) * Time.deltaTime;
    //    }
    //    else if (rigidbody.velocity.y > 0 && !Input.GetKey(KeyCode.UpArrow))
    //    {
    //        rigidbody.velocity += Vector2.up * Physics2D.gravity.y * (low_jump - 1) * Time.deltaTime;
    //    }

    //}
    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.gameObject.tag == "ground")
    //    {
    //        touch = true;
    //    }
    //}
}
