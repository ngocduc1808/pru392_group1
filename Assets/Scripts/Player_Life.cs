using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player_Life : MonoBehaviour
{
    // Start is called before the first frame update
    private Animator animator;
    private Rigidbody2D rb;
    public int damage;
    int currentHealth;
    public HealthBar healthBar;
    [SerializeField] int maxHealth;

    private void Start()
    {
        animator = GetComponent<Animator>();
        rb=GetComponent<Rigidbody2D>();
        currentHealth = maxHealth;
        healthBar.UpdateBar(currentHealth, maxHealth);
    }

    private enum MovementState { idle, running, jumping, falling, hit };
    private MovementState movementState = MovementState.idle;

    // Update is called once per frame
    void Update()
    {
        animator.SetBool("isHit", false);
        animator.SetBool("isHit3",false);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("trap"))
        {
            //  Die();
            animator.SetTrigger("hit");
            currentHealth -= damage;
            if (currentHealth == 0)
            {
                // Destroy(this.gameObject);
                animator.SetTrigger("death");
                SceneManager.LoadScene("Death");
            }
            healthBar.UpdateBar(currentHealth, maxHealth);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("trap2"))
        {
            //  Die();
            animator.SetTrigger("hit");
            currentHealth -= damage;
            if (currentHealth == 0)
            {
                // Destroy(this.gameObject);
                animator.SetTrigger("death");
                SceneManager.LoadScene("Death");
            }
            healthBar.UpdateBar(currentHealth, maxHealth);
        }
    }

    //void takeDamage(int damage)
    //{
    //        currentHealth -= damage;
    //        if (currentHealth == 0)
    //        {
    //            // Destroy(this.gameObject);
    //            animator.SetTrigger("death");
    //        }
    //        healthBar.UpdateBar(currentHealth, maxHealth);
    //}

    //private void Die()
    //{
    //    rb.bodyType = RigidbodyType2D.Static;
    //    animator.SetTrigger("death");
    //}
    private void ResetLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
