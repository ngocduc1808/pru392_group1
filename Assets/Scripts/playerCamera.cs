using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerCamera : MonoBehaviour
{
    public float followSpeed = 2f;
    public Transform target;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 newPos = new Vector3(target.position.x+3, target.position.y+1, -1f);
        transform.position= Vector3.Slerp(transform.position,newPos,followSpeed);
    }
}
