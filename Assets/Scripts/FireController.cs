using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireController : MonoBehaviour
{
    public float fireSpped;
    Rigidbody2D mybody;

    private void Awake()
    {
        mybody= GetComponent<Rigidbody2D>();
        if (transform.localRotation.z > 0)
        {
            mybody.AddForce(new Vector2(-1, 0) * fireSpped, ForceMode2D.Impulse);

        }
        else
        {
            mybody.AddForce(new Vector2(1, 0) * fireSpped, ForceMode2D.Impulse);

        }
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
