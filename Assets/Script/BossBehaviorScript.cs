using System.Threading;
using UnityEngine;

public class BossBehaviorScript : MonoBehaviour
{
    Rigidbody2D enemyRigidbody;
    Transform target;
    public float timeCount = 0;
    bool isFacingRight = true;
    float speed = 6f;
    int hitPoint = 0;
    public float distance = 0f;
    public float velocity = 0f;
    public bool isFollow = false;

    [SerializeField] private Transform firePoint;

    public GameObject fire;
    public float nextFire = 3f;

    // Start is called before the first frame update
    void Start()
    {
        enemyRigidbody = GetComponent<Rigidbody2D>();
        FindTarget();
    }

    // Update is called once per frame
    void Update()
    {
        enemyRigidbody.velocity = Vector3.zero;
        distance = target.transform.position.x - transform.position.x;
        isFollow = true;
        FollowPlayer();
        FaceAt(target.transform.position.x - transform.position.x < 0);
        Resume();
        EnemyDie();
        shoot();
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Wall")
        {
            transform.localScale = new Vector2(20 * -(Mathf.Sign(enemyRigidbody.velocity.x)), transform.localScale.y);
            isFacingRight = !isFacingRight;
            isFollow = false;
        }

        if (collision.gameObject.tag == "fireball")
        {
            hitPoint++;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
            Stop();
        }
    }

    private void FixedUpdate()
    {
        
    }

    void shoot()
    {
        if (Time.time >= timeCount + nextFire)
        {
            Debug.Log(timeCount);
            timeCount = Time.time;
            if (isFacingRight)
            {
                Instantiate(fire, firePoint.position, Quaternion.Euler(new Vector3(0, 0, 0)));
            }
            else if (!isFacingRight)
            {
                Instantiate(fire, firePoint.position, Quaternion.Euler(new Vector3(0, 0, 180)));

            }         
        }
        
    }


    private void FindTarget()
    {
        if (target == null)
        {
            target = GameObject.FindWithTag("Player").GetComponent<Transform>();
        }
    }

    private void FollowPlayer()
    {
        transform.position = Vector2.MoveTowards(transform.position, new Vector2(target.transform.position.x, transform.position.y), (speed) * Time.deltaTime);
    }

    private void EnemyMove()
    {
        if (isFacingRight)
        {
            // move right
            enemyRigidbody.velocity = new Vector2(speed, 0f);
        }
        else
        {
            // move left
            enemyRigidbody.velocity = new Vector2(-speed, 0f);
        }
    }

    private void EnemyDie()
    {
        if (hitPoint == 30)
        {
            Destroy(gameObject);
        }
    }

    private void FaceAt(bool condition)
    {
        Vector3 scale = transform.localScale;
        if (condition)
        {
            scale.x = Mathf.Abs(scale.x) * -1;
            isFacingRight = false;
        }
        else
        {
            scale.x = Mathf.Abs(scale.x);
            isFacingRight = true;
        }

        transform.localScale = scale;
    }

    private void Stop()
    {
        speed = 0f;
    }

    private void Resume()
    {
        speed = 1.5f;
    }
}
