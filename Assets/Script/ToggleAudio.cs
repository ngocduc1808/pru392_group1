using UnityEngine;

public class ToggleAudio : MonoBehaviour
{
    [SerializeField] private bool _toggleMusic;
    public void Toggle()
    {
        if (_toggleMusic) SoundManager.Instance.ToggleMusic();
    }

}
