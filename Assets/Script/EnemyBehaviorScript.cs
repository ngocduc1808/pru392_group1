using UnityEngine;

public class EnemyBehaviorScript : MonoBehaviour
{
    Rigidbody2D enemyRigidbody;
    Transform target;
    Animator animator;
    bool isFacingRight = true;
    float speed = 3f;
    int hitPoint = 0;
    public float distance = 0f;
    public float velocity = 0f;
    public bool isFollow = false;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        enemyRigidbody = GetComponent<Rigidbody2D>();
        FindTarget();
    }

    // Update is called once per frame
    void Update()
    {
        if (Mathf.Abs(target.position.x - transform.position.x) <= 5 && Mathf.Abs(target.position.y - transform.position.y) <= 0.35)
        {
            enemyRigidbody.velocity = Vector3.zero;
            distance = target.transform.position.x - transform.position.x;
            isFollow = true;
            FollowPlayer();
            FaceAt(target.transform.position.x - transform.position.x < 0);
        }
        else
        {
            velocity = enemyRigidbody.velocity.x;
            isFollow = false;
            EnemyMove();
            FaceAt(Mathf.Sign(enemyRigidbody.velocity.x) < 0);
            Resume();
        }

        EnemyDie();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Wall")
        {
            transform.localScale = new Vector2(6 * -(Mathf.Sign(enemyRigidbody.velocity.x)), transform.localScale.y);
            isFacingRight = !isFacingRight;
            isFollow = false;
        }

        if (collision.gameObject.tag == "fireball")
        {
            animator.SetBool("isHit", true);
            hitPoint++;
        }
        else
        {
            animator.SetBool("isHit", false);
        }

        if (collision.gameObject.tag == "Player")
        {
            animator.Play("dieNew Animation");
            Destroy(gameObject, 1f);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
            Stop();
        }
    }

    private void FindTarget()
    {
        if (target == null)
        {
            target = GameObject.FindWithTag("Player").GetComponent<Transform>();
        }
    }

    private void FollowPlayer()
    {
        transform.position = Vector2.MoveTowards(transform.position, new Vector2(target.transform.position.x, transform.position.y), (speed + 3f) * Time.deltaTime);
    }

    private void EnemyMove()
    {
        if (isFacingRight)
        {
            // move right
            enemyRigidbody.velocity = new Vector2(speed, 0f);
        }
        else
        {
            // move left
            enemyRigidbody.velocity = new Vector2(-speed, 0f);
        }
    }

    private void EnemyDie()
    {
        if (hitPoint == 3)
        {
            animator.Play("dieNew Animation");
            Destroy(gameObject, 1f);
        }
    }

    private void FaceAt(bool condition)
    {
        Vector3 scale = transform.localScale;
        if (condition)
        {
            scale.x = Mathf.Abs(scale.x) * -1;
            isFacingRight = false;
        }
        else
        {
            scale.x = Mathf.Abs(scale.x);
            isFacingRight = true;
        }

        transform.localScale = scale;
    }

    private void Stop()
    {
        speed = 0f;
    }

    private void Resume()
    {
        speed = 1.5f;
    }
}
