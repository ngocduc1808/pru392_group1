using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SwitchScene()
    {
        SceneManager.LoadScene("MainScene");
    }

    public void MoveLeft()
    {

    }

    public void MoveRight()
    {

    }

    public void Jump()
    {

    }

    public void Fire()
    {

    }
}
