
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Main_Player2 : MonoBehaviour
{
    // Start is called before the first frame update
    Rigidbody2D rb;
    private Animator animator;
    private SpriteRenderer spriteRenderer;
    private BoxCollider2D coll;
    public TextMeshProUGUI text;
    private int count;
    private float dirx = 0f;
    private bool isfacingRight = true;
    [SerializeField] private LayerMask jumpableGround;
    [SerializeField] private float moveSpeed = 7f;
    [SerializeField] private float jumpForce = 14f;


    [SerializeField] private Transform firePoint;
    [SerializeField] private GameObject[] fireballs;

    public Transform gunTip;
    public GameObject fire;
    float fireRate = 0.2f;
    float nextFire = 0f;

    private enum MovementState { idle, running, jumping, falling,hit };
    private MovementState movementState = MovementState.idle;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        coll = GetComponent<BoxCollider2D>();

    }

    // Update is called once per frame
    void Update()
    {
        dirx = Input.GetAxisRaw("Horizontal");
        rb.velocity = new Vector2(dirx * 7f, rb.velocity.y);

        if (Input.GetButtonDown("Jump") && isGrounded())
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        }
        UpdateAnimationUpdate();
        if (Input.GetMouseButton(0))
            shoot();
        // flip();


    }
    private void FixedUpdate()
    {
        if (Input.GetAxisRaw("Fire1") > 0) { fireStat(); }
    }

    private void UpdateAnimationUpdate()
    {
        MovementState state;

        if (dirx > 0f && !isfacingRight || dirx < 0f && isfacingRight)
        {
            state = MovementState.running;

            isfacingRight = !isfacingRight;

            // spriteRenderer.flipX = false;
            Vector3 n = transform.localScale;
            n.x *= -1;
            transform.localScale = n;
        }
        else if (dirx > 0f && isfacingRight || dirx < 0f && !isfacingRight)
        {
            state = MovementState.running;
        }

        //else if (dirx < 0f && isfacingRight)
        //{
        //    state = MovementState.running;

        //    isfacingRight = !isfacingRight;

        //  // spriteRenderer.flipX = true;
        //    Vector3 n = transform.localScale;
        //    n.x *= -1;
        //    transform.localScale = n;
        //}
        else
        {
            state = MovementState.idle;

        }
        if (rb.velocity.y > .1f)
        {
            state = MovementState.jumping;

        }
        else if (rb.velocity.y < -.1f)
        {
            state = MovementState.falling;
        }
        animator.SetInteger("state", (int)state);

    }
    private bool isGrounded()
    {
        return Physics2D.BoxCast(coll.bounds.center, coll.bounds.size, 0f, Vector2.down, .1f, jumpableGround);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("fruit"))
        {
            Destroy(collision.gameObject);
            count++;
            text.text = "Apple: " + count;
        }

        if(collision.gameObject.tag == "flag")
        {
            SceneManager.LoadScene("MainScene2");
        }
    }
    private void shoot()
    {
        fireballs[0].transform.position = firePoint.position;
        fireballs[0].GetComponent<Ball_Fire>().SetDirection(Mathf.Sign(transform.localScale.x));
    }


    void fireStat()
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            if (isfacingRight)
            {
                Instantiate(fire, gunTip.position, Quaternion.Euler(new Vector3(0, 0, 0)));
            }
            else if(!isfacingRight)
            {
                Instantiate(fire, gunTip.position, Quaternion.Euler(new Vector3(0, 0, 180)));

            }
        }
    }

}
